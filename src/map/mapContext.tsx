import React, { createContext, FunctionComponent, Dispatch } from "react";
import { useReducer } from "react";
import { IGeoLocation, IKommune, IOption } from "./interfaces";
import { mapReducer, MapAction } from "./mapReducer";

export interface IMapState {
  locations: IGeoLocation[];
  kommuner: IKommune[];
  selectedDateringer: IOption[];
  selectedKommune: IOption | null;
}

const intialState: IMapState = {
  locations: [],
  kommuner: [],
  selectedDateringer: [],
  selectedKommune: null,
};

export const MapContext = createContext<{
  state: IMapState;
  dispatch: Dispatch<MapAction>;
}>({ state: intialState, dispatch: () => null });

export const MapProvider: FunctionComponent = ({ children }) => {
  const [state, dispatch] = useReducer(mapReducer, intialState);
  return (
    <MapContext.Provider value={{ state, dispatch }}>
      {children}
    </MapContext.Provider>
  );
};
