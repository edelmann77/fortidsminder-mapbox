import { useContext, useEffect } from "react";
import { MapContext } from "./mapContext";
import { getLocationsByBounds } from "../util/api";
import { IGeoLocation } from "./interfaces";

export const useZoomHandler = () => {
  const mapContext = useContext(MapContext);
  const handleZoomOrBoundsChanged = async (zoom: number, bounds: number[]) => {
    if (zoom > 10) {
      const locations = await getLocationsByBounds(bounds);
      mapContext.dispatch({ type: "addLocations", locations });
    } else {
      mapContext.dispatch({ type: "addLocations", locations: [] });
    }
  };

  return { handleZoomOrBoundsChanged };
};

export const useSourceAndLayerHandler = (
  map: mapboxgl.Map | null,
  locations: IGeoLocation[],
  isInitializing: boolean
) => {
  const locationsSource = "locationsSource";
  const locationsLayer = "locationsLayer";

  useEffect(() => {
    if (!map || isInitializing) {
      return;
    }
    const source = map.getSource(locationsSource);
    const layer = map.getLayer(locationsLayer);

    if (layer) {
      map.removeLayer(locationsLayer);
    }
    if (source) {
      map.removeSource(locationsSource);
    }

    map.addSource(locationsSource, {
      type: "geojson",
      data: { type: "FeatureCollection", features: locations },
    });

    map.addLayer({
      id: locationsLayer,
      type: "circle",
      source: locationsSource,
      paint: {
        "circle-radius": 5,
        "circle-color": [
          "case",
          ["to-boolean", ["get", "fredNr"]],
          "red",
          "blue",
        ],
      },
    });
  }, [locations, map, isInitializing]);

  return { locationsSource, locationsLayer };
};
