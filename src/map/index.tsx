import React, {
  FunctionComponent,
  useContext,
  useEffect,
  useState,
} from "react";
import MapComponent from "./mapComponent";
import { useZoomHandler } from "./hooks";
import { MapContext } from "./mapContext";

const Map: FunctionComponent = () => {
  const [panTo, setPanTo] = useState<number[] | undefined>();
  const { handleZoomOrBoundsChanged } = useZoomHandler();

  const {
    locations,
    selectedDateringer,
    selectedKommune,
    kommuner,
  } = useContext(MapContext).state;

  const dateringer = selectedDateringer.map((d) => d.name);
  const filteredLocations =
    selectedDateringer.length > 0
      ? locations.filter((l) => dateringer.includes(l.properties.datering))
      : locations;

  useEffect(() => {
    if (kommuner) {
      if (selectedKommune) {
        const centerKommune = kommuner.find((k) => k.id === selectedKommune.id);
        if (centerKommune) {
          setPanTo(centerKommune.visueltcenter);
        }
      } else {
        setPanTo(undefined);
      }
    }
  }, [selectedKommune, kommuner]);

  return (
    <MapComponent
      locations={filteredLocations}
      onZoomOrBoundsChanged={handleZoomOrBoundsChanged}
      panTo={panTo}
    />
  );
};

export default Map;
