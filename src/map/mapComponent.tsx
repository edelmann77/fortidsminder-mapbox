import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import mapboxgl from "mapbox-gl";
import "./style.css";
import { useSourceAndLayerHandler } from "./hooks";
import { IGeoLocation, ILocation } from "./interfaces";
import ControlPanel from "../controlPanel";

interface IProps {
  locations: IGeoLocation[];
  onZoomOrBoundsChanged: (zoom: number, bounds: number[]) => void;
  panTo: number[] | undefined;
}

enum MapStyle {
  Satellite = "mapbox://styles/mapbox/satellite-streets-v11",
  Plain = "mapbox://styles/mapbox/streets-v11",
}

const MapComponent: FunctionComponent<IProps> = ({
  onZoomOrBoundsChanged,
  locations,
  panTo,
}) => {
  const mapContainer = useRef<HTMLDivElement | null>(null);
  const map = useRef<mapboxgl.Map | null>(null);
  const selectedMapStyle = useRef<MapStyle>(MapStyle.Satellite);

  const [isInitializing, setIsInitializing] = useState(true);

  const { locationsLayer } = useSourceAndLayerHandler(
    map.current,
    locations,
    isInitializing
  );

  useEffect(() => {
    const m = map.current;
    if (panTo && m) {
      m.flyTo({
        center: { lat: panTo[1], lng: panTo[0] },
        zoom: 11
      })
    }
  }, [panTo]);

  useEffect(() => {
    const handleMapChanges = () => {
      const bbox: number[] = [
        ...map.current!.getBounds().getNorthEast().toArray(),
        ...map.current!.getBounds().getSouthWest().toArray(),
      ];
      onZoomOrBoundsChanged(map.current!.getZoom(), bbox);

      // if (
      //   map.current!.getZoom() > 14.7 &&
      //   selectedMapStyle.current !== MapStyle.Satellite
      // ) {
      //   selectedMapStyle.current = MapStyle.Satellite;
      //   map.current!.setStyle(MapStyle.Satellite);
      // } else if (
      //   map.current!.getZoom() <= 14.7 &&
      //   selectedMapStyle.current !== MapStyle.Plain
      // ) {
      //   selectedMapStyle.current = MapStyle.Plain;
      //   map.current!.setStyle(MapStyle.Plain);
      // }
    };

    if (!map.current) {
      map.current = new mapboxgl.Map({
        container: mapContainer.current ?? "",
        style: selectedMapStyle.current,
        bounds: [
          16.73356562236893,
          58.045415372194725,
          5.32004710530984,
          54.17412229110002,
        ],
        zoom: 7.035819107698143,
      });

      map.current.on("load", () => setIsInitializing(false));
      map.current.on("moveend", handleMapChanges);
      map.current.on("pitchend", handleMapChanges);
      map.current.on("mouseenter", locationsLayer, () => {
        const m = map.current;
        if (m) {
          m.getCanvas().style.cursor = "pointer";
        }
      });
      map.current.on("mouseleave", locationsLayer, () => {
        const m = map.current;
        if (m) {
          m.getCanvas().style.cursor = "";
        }
      });
      map.current.on("click", locationsLayer, (e: any) => {
        const m = map.current;

        if (m) {
          const coordinates = e.features[0].geometry.coordinates;
          const location = e.features[0].properties as ILocation;

          const popupContent = location
            ? `<b>${location.stednavn}</b>
          <br />${location.anlaegsbetydning}
          <br />${location.datering}
          <br /><a href="http://www.kulturarv.dk/fundogfortidsminder/Lokalitet/${location.systemnr}" target="_blank">Fund of fortidsminder</a>`
            : "";

          new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML(popupContent)
            .addTo(m);
        }
      });
    }
  }, [onZoomOrBoundsChanged, locationsLayer]);

  return (
    <>
      <ControlPanel />
      <div ref={(el) => (mapContainer.current = el)} />
    </>
  );
};

export default MapComponent;
