import { Feature, Geometry } from "geojson";

export interface IKommune {
  id: number;
  kommuneName: string;
  visueltcenter: number[];
}
export interface ILocation {
  anlaegsbetydning: string;
  datering: string;
  fredNr: string;
  stednavn: string;
  systemnr: number;
}

export interface IApiLocation extends ILocation {
  x: number;
  y: number;
}

export interface IGeoLocation extends Feature<Geometry, ILocation> {}

export interface IOption {
  id: number;
  name: string;
}
