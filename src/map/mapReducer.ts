import { IMapState } from "./mapContext";
import { IApiLocation, IOption } from "./interfaces";

export type MapAction =
  | { type: "addLocations"; locations: IApiLocation[] }
  | { type: "addKommuner"; kommuner: any[] }
  | { type: "setSelectedDateringer"; dateringer: IOption[] }
  | { type: "setSelectedKommune"; kommune: IOption };

export const mapReducer = (state: IMapState, action: MapAction): IMapState => {
  switch (action.type) {
    case "addLocations": {
      return {
        ...state,
        locations: action.locations.map((l) => ({
          geometry: {
            coordinates: [l.x, l.y],
            geometries: [],
            type: "Point",
          },
          properties: {
            anlaegsbetydning: l.anlaegsbetydning,
            datering: l.datering,
            fredNr: l.fredNr,
            stednavn: l.stednavn,
            systemnr: l.systemnr,
          },
          type: "Feature",
        })),
      };
    }
    case "addKommuner": {
      return {
        ...state,
        kommuner: action.kommuner.map((k, i) => ({
          id: i,
          kommuneName: k.navn,
          visueltcenter: k.visueltcenter,
        })),
      };
    }
    case "setSelectedDateringer": {
      return {
        ...state,
        selectedDateringer: action.dateringer,
      };
    }
    case "setSelectedKommune": {
      return {
        ...state,
        selectedKommune: action.kommune,
      };
    }
    default: {
      return state;
    }
  }
};
