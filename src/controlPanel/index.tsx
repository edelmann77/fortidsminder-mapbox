import React, { FunctionComponent, useEffect, useContext } from "react";

import "./style.css";
import "react-bootstrap-typeahead/css/Typeahead.css";

import { MapContext } from "../map/mapContext";
import { getKommuner } from "../util/api";
import ControlPanelComponent from "./controlPanelComponent";
import { IOption } from "../map/interfaces";

const ControlPanel: FunctionComponent = () => {
  const mapDispatcher = useContext(MapContext).dispatch;
  const { kommuner} = useContext(MapContext).state;

  useEffect(() => {
    const fetch = async () => {
      const kommuner = await getKommuner();
      mapDispatcher({ type: "addKommuner", kommuner });
    };
    fetch();
  }, [mapDispatcher]);

  const dateringOptions: string[] = [
    "Udateret",
    "Oldtid",
    "Stenalder",
    "Bronzealder",
    "Jernalder",
    "Vikingetid",
    "Historisk Tid",
    "Middelalder",
    "Efterreformatorisk tid",
    "Nyere tid",
  ];

  const sortedKommuner = kommuner.sort((a, b) =>
    a.kommuneName > b.kommuneName ? 1 : -1
  );

  return (
    <ControlPanelComponent
      kommuner={sortedKommuner}
      dateringer={dateringOptions}
      onDateringChanged={(options: IOption[]) =>
        mapDispatcher({ type: "setSelectedDateringer", dateringer: options })
      }
      onKommuneChanged={(option: IOption) =>
        mapDispatcher({ type: "setSelectedKommune", kommune: option })
      }
    />
  );
};

export default ControlPanel;
