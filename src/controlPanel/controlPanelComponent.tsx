import React, { FunctionComponent, useState } from "react";
import { Typeahead } from "react-bootstrap-typeahead";
import { IKommune, IOption } from "../map/interfaces";

interface IProps {
  kommuner: IKommune[];
  dateringer: string[];
  onDateringChanged: (options: IOption[]) => void;
  onKommuneChanged: (option: IOption) => void;
}

const ControlPanelComponent: FunctionComponent<IProps> = ({
  kommuner,
  dateringer,
  onDateringChanged,
  onKommuneChanged,
}) => {
  const [collapsed, setCollapsed] = useState(false);

  const kommuneOptions = kommuner.map((k, i) => ({
    id: k.id,
    name: k.kommuneName,
  }));

  const dateringsOptions = dateringer.map((d, i) => ({ id: i, name: d }));

  return (
    <div className={`Sidebar ${collapsed ? "Sidebar--collapsed" : ""}`}>
      <div className="SidebarHeader">
        Indstillinger
        <svg
          className={`Sidebar__Arrow ${
            collapsed ? "Sidebar__Arrow--rotate" : ""
          }`}
          onClick={() => setCollapsed(!collapsed)}
          viewBox="0 0 500 500"
        >
          <polygon points="396.6,160 416,180.7 256,352 96,180.7 115.3,160 256,310.5 " />
        </svg>
      </div>
      <div
        className={`SidebarContent ${
          collapsed ? "SidebarContent-collapsed" : ""
        }`}
      >
        <div className="SidebarItem">
          <Typeahead
            id="Typeahead1"
            labelKey={(option: IOption) => `${option.name}`}
            multiple={false}
            options={kommuneOptions}
            placeholder="Vælg en kommune"
            emptyLabel="Ingen kommune fundet"
            onChange={(options) => onKommuneChanged(options[0] || null)}
            selectHintOnEnter={true}
            clearButton={true}
          />
        </div>
        <div className="SidebarItem">
          <Typeahead
            id="Typeahead2"
            labelKey={(option: IOption) => `${option.name}`}
            multiple={true}
            options={dateringsOptions}
            placeholder="Vælg en datering"
            emptyLabel="Ingen datering fundet"
            onChange={onDateringChanged}
            selectHintOnEnter={true}
            clearButton={true}
          />
        </div>
      </div>
      <div className="SidebarFooter">&copy; Mikkel Edelmann Nielsen 2020</div>
    </div>
  );
};

export default ControlPanelComponent;
