let abortController = new AbortController();
let isFetching = false;

export const getLocationsByBounds = async (bounds: number[]) => {
  if (isFetching) {
    abortController.abort();
    abortController = new AbortController();
  }

  isFetching = true;

  return await window
    .fetch(
      // `http://localhost:8000/locations?y1=${bounds[1]}&x1=${bounds[0]}&y2=${bounds[3]}&x2=${bounds[2]}`,
      `http://edelmann-web.dk/api/fortidsminder/locations?y1=${bounds[1]}&x1=${bounds[0]}&y2=${bounds[3]}&x2=${bounds[2]}`,
      {
        method: "get",
        signal: abortController.signal,
      }
    )
    .then((response) => {
      isFetching = false;
      return response;
    })
    .then((response) => response.json())
    .catch((e: any) => []);
};

export const getKommuner = async () => {
  const url = "https://dawa.aws.dk/kommuner";

  return await window.fetch(url).then((response) => response.json());
};
