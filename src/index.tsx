import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import Map from "./map";
import mapboxgl from "mapbox-gl";
import { MapProvider } from "./map/mapContext";

mapboxgl.accessToken =
  "pk.eyJ1IjoibWVuNzciLCJhIjoiY2tibWp3NnhoMWpidDJzcXY5NndhNWRxdSJ9.cqyyc7GRKDEMctHzHV8AIA";

ReactDOM.render(
  <React.StrictMode>
    <MapProvider>
      <Map />
    </MapProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
